﻿namespace ExaminationTicket.GUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuTop = new System.Windows.Forms.MenuStrip();
            this.mtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.mtNewExamination = new System.Windows.Forms.ToolStripMenuItem();
            this.mtExportTxt = new System.Windows.Forms.ToolStripMenuItem();
            this.mtSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mtExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mtAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.mtPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPullTicket = new System.Windows.Forms.Button();
            this.tbFIO = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lvTikets = new System.Windows.Forms.ListView();
            this.number = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.variant = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.description = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.menuTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuTop
            // 
            this.menuTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mtMenu,
            this.mtAbout,
            this.mtPrint});
            this.menuTop.Location = new System.Drawing.Point(0, 0);
            this.menuTop.Name = "menuTop";
            this.menuTop.Size = new System.Drawing.Size(883, 24);
            this.menuTop.TabIndex = 0;
            this.menuTop.Text = "menuTop";
            // 
            // mtMenu
            // 
            this.mtMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mtNewExamination,
            this.mtExportTxt,
            this.mtSettings,
            this.toolStripSeparator1,
            this.mtExit});
            this.mtMenu.Name = "mtMenu";
            this.mtMenu.Size = new System.Drawing.Size(53, 20);
            this.mtMenu.Text = "Меню";
            // 
            // mtNewExamination
            // 
            this.mtNewExamination.Name = "mtNewExamination";
            this.mtNewExamination.Size = new System.Drawing.Size(167, 22);
            this.mtNewExamination.Text = "Новый экзамен";
            this.mtNewExamination.Click += new System.EventHandler(this.mtNewExamination_Click);
            // 
            // mtExportTxt
            // 
            this.mtExportTxt.Name = "mtExportTxt";
            this.mtExportTxt.Size = new System.Drawing.Size(167, 22);
            this.mtExportTxt.Text = "Экспорт билетов";
            this.mtExportTxt.Click += new System.EventHandler(this.mtExportTxt_Click);
            // 
            // mtSettings
            // 
            this.mtSettings.Name = "mtSettings";
            this.mtSettings.Size = new System.Drawing.Size(167, 22);
            this.mtSettings.Text = "Настройки";
            this.mtSettings.Click += new System.EventHandler(this.mtSettings_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(164, 6);
            // 
            // mtExit
            // 
            this.mtExit.Name = "mtExit";
            this.mtExit.Size = new System.Drawing.Size(167, 22);
            this.mtExit.Text = "Выход";
            this.mtExit.Click += new System.EventHandler(this.mtExit_Click);
            // 
            // mtAbout
            // 
            this.mtAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.mtAbout.Name = "mtAbout";
            this.mtAbout.Size = new System.Drawing.Size(97, 20);
            this.mtAbout.Text = "О программе!";
            this.mtAbout.Click += new System.EventHandler(this.mtAbout_Click);
            // 
            // mtPrint
            // 
            this.mtPrint.Name = "mtPrint";
            this.mtPrint.Size = new System.Drawing.Size(58, 20);
            this.mtPrint.Text = "Печать";
            this.mtPrint.Click += new System.EventHandler(this.mtPrint_Click);
            // 
            // btnPullTicket
            // 
            this.btnPullTicket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPullTicket.Enabled = false;
            this.btnPullTicket.Location = new System.Drawing.Point(777, 28);
            this.btnPullTicket.Name = "btnPullTicket";
            this.btnPullTicket.Size = new System.Drawing.Size(94, 23);
            this.btnPullTicket.TabIndex = 2;
            this.btnPullTicket.Text = "Вытянуть билет!";
            this.btnPullTicket.UseVisualStyleBackColor = true;
            this.btnPullTicket.Click += new System.EventHandler(this.btnPullTicket_Click);
            // 
            // tbFIO
            // 
            this.tbFIO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFIO.Location = new System.Drawing.Point(55, 30);
            this.tbFIO.Name = "tbFIO";
            this.tbFIO.Size = new System.Drawing.Size(716, 20);
            this.tbFIO.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "ФИО:";
            // 
            // lvTikets
            // 
            this.lvTikets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvTikets.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.number,
            this.variant,
            this.description});
            this.lvTikets.LabelWrap = false;
            this.lvTikets.Location = new System.Drawing.Point(13, 67);
            this.lvTikets.MultiSelect = false;
            this.lvTikets.Name = "lvTikets";
            this.lvTikets.Size = new System.Drawing.Size(858, 570);
            this.lvTikets.TabIndex = 5;
            this.lvTikets.UseCompatibleStateImageBehavior = false;
            this.lvTikets.View = System.Windows.Forms.View.Details;
            // 
            // number
            // 
            this.number.Text = "Номер вопроса";
            this.number.Width = 97;
            // 
            // variant
            // 
            this.variant.Text = "Номер варианта";
            this.variant.Width = 97;
            // 
            // description
            // 
            this.description.Text = "Вопрос";
            this.description.Width = 650;
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "txt";
            this.saveFileDialog.Filter = "Текстовый файл (*.txt)|*.txt";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(883, 649);
            this.Controls.Add(this.lvTikets);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbFIO);
            this.Controls.Add(this.btnPullTicket);
            this.Controls.Add(this.menuTop);
            this.MainMenuStrip = this.menuTop;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Экзаменационный билет";
            this.menuTop.ResumeLayout(false);
            this.menuTop.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuTop;
        private System.Windows.Forms.ToolStripMenuItem mtMenu;
        private System.Windows.Forms.ToolStripMenuItem mtSettings;
        private System.Windows.Forms.ToolStripMenuItem mtExit;
        private System.Windows.Forms.ToolStripMenuItem mtAbout;
        private System.Windows.Forms.ToolStripMenuItem mtPrint;
        private System.Windows.Forms.Button btnPullTicket;
        private System.Windows.Forms.TextBox tbFIO;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lvTikets;
        private System.Windows.Forms.ColumnHeader number;
        private System.Windows.Forms.ColumnHeader variant;
        private System.Windows.Forms.ColumnHeader description;
        private System.Windows.Forms.ToolStripMenuItem mtNewExamination;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mtExportTxt;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}

