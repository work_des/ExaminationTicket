﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;
using ExaminationTicket.Model;
using ExaminationTicket.Services;

namespace ExaminationTicket.GUI
{
    public partial class MainForm : Form
    {
        internal static readonly string PatchSettings = Application.UserAppDataPath + "\\settings.json";
        internal static readonly string PatchExamination = Application.UserAppDataPath + "\\tickets.json";

        private readonly Examination examination = new Examination();
        private readonly PrintDocument printDocument = new PrintDocument();

        private string textToPrint;
        private string documentContents;

        public MainForm()
        {
            InitializeComponent();

            Helper.TryCatch(() =>
            {
                examination.StudentsTickets.ForEach(AddTicketsInView);

                btnPullTicket.Enabled = examination.QuestionsAll != null;
            });

            printDocument.PrintPage += PrintDocument_PrintPage;
        }

        /// <summary>
        ///     Добавление вытянутых билетов в ListView
        /// </summary>
        /// <param name="ticket">билет</param>
        private void AddTicketsInView(StudentTicket ticket)
        {
            var group = new ListViewGroup(ticket.Fio);

            lvTikets.Groups.Add(group);

            foreach (var item in ticket.QuestionsList)
            {
                var itemView = new ListViewItem(item.Number, group);
                itemView.SubItems.Add(item.Variant);
                itemView.SubItems.Add(item.Description);
                lvTikets.Items.Add(itemView);
            }
        }

        private void mtSettings_Click(object sender, EventArgs e)
        {
            Helper.TryCatch(() => { new SettingsForm().ShowDialog(); });
        }

        private void mtPrint_Click(object sender, EventArgs e)
        {
            Helper.ExportToTxt(examination, $"{Application.StartupPath}\\Examination.txt");

            textToPrint = documentContents = Helper.ReadFile($"{Application.StartupPath}\\Examination.txt");

            var printPrvDlg = new PrintPreviewDialog {Document = printDocument};

            printPrvDlg.ShowDialog(this);
        }

        private void PrintDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            int charactersOnPage, linesPerPage;

            e.Graphics.MeasureString(textToPrint, Font, e.MarginBounds.Size, StringFormat.GenericTypographic,
                out charactersOnPage, out linesPerPage);

            // Рисуем строку в пределах страницы
            e.Graphics.DrawString(textToPrint, Font, Brushes.Black, e.MarginBounds, StringFormat.GenericTypographic);

            // Удаляем часть строки которая была напечатана
            textToPrint = textToPrint.Substring(charactersOnPage);

            // Проверяем должны ли печататься еще страницы
            e.HasMorePages = textToPrint.Length > 0;

            // Сброс строк для печати если страниц больше нет
            if (!e.HasMorePages)
                textToPrint = documentContents;
        }

        private void mtAbout_Click(object sender, EventArgs e)
        {
            new AboutForm().ShowDialog();
        }

        private void mtExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnPullTicket_Click(object sender, EventArgs e)
        {
            Helper.TryCatch(() =>
            {
                if (!File.Exists(PatchSettings))
                    MessageBox.Show("В настройках отсутствуют файлы с вопросами!", "Предупреждение!",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else if (string.IsNullOrEmpty(tbFIO.Text))
                    MessageBox.Show("Введите ФИО студента!", "Предупреждение!", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                else
                {
                    AddTicketsInView(examination.PullTicket(tbFIO.Text));

                    tbFIO.Text = "";
                }
            });
        }

        private void mtNewExamination_Click(object sender, EventArgs e)
        {
            if (!File.Exists(PatchSettings))
                MessageBox.Show("В настройках отсутствуют файлы с вопросами!", "Предупреждение!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                if (MessageBox.Show("Начать новый экзамен? \nВсе экзаменационные билеты будут сброшены.", "Внимание!!!", 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    examination.NewExamination();
                    lvTikets.Items.Clear();

                    btnPullTicket.Enabled = true;
                }
            }
        }

        private void mtExportTxt_Click(object sender, EventArgs e)
        {
            Helper.TryCatch(() =>
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    Helper.ExportToTxt(examination, saveFileDialog.FileName);

                MessageBox.Show("Билеты успешно экспортированны!", "Экспорт");
            });
        }
    }
}