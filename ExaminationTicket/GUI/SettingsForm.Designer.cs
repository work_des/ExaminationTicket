﻿namespace ExaminationTicket.GUI
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.lwFiles = new System.Windows.Forms.ListView();
            this.number = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.patchFile = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnRemoveFiles = new System.Windows.Forms.Button();
            this.btnAddFiles = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(104, 221);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Отмена";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Enabled = false;
            this.btnSaveSettings.Location = new System.Drawing.Point(251, 221);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(75, 23);
            this.btnSaveSettings.TabIndex = 1;
            this.btnSaveSettings.Text = "Сохранить";
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "Выбор файла";
            this.openFileDialog.Filter = "files|*.txt";
            this.openFileDialog.Multiselect = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Список файлов с вопросами";
            // 
            // lwFiles
            // 
            this.lwFiles.AllowDrop = true;
            this.lwFiles.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.number,
            this.patchFile});
            this.lwFiles.FullRowSelect = true;
            this.lwFiles.GridLines = true;
            this.lwFiles.Location = new System.Drawing.Point(13, 26);
            this.lwFiles.MultiSelect = false;
            this.lwFiles.Name = "lwFiles";
            this.lwFiles.Size = new System.Drawing.Size(404, 160);
            this.lwFiles.TabIndex = 6;
            this.lwFiles.UseCompatibleStateImageBehavior = false;
            this.lwFiles.View = System.Windows.Forms.View.Details;
            // 
            // number
            // 
            this.number.Text = "Номер вопроса";
            this.number.Width = 100;
            // 
            // patchFile
            // 
            this.patchFile.Text = "Файл вопросов";
            this.patchFile.Width = 300;
            // 
            // btnRemoveFiles
            // 
            this.btnRemoveFiles.BackColor = System.Drawing.Color.Transparent;
            this.btnRemoveFiles.FlatAppearance.BorderSize = 0;
            this.btnRemoveFiles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoveFiles.Image = global::ExaminationTicket.Properties.Resources.delete;
            this.btnRemoveFiles.Location = new System.Drawing.Point(52, 189);
            this.btnRemoveFiles.Name = "btnRemoveFiles";
            this.btnRemoveFiles.Size = new System.Drawing.Size(29, 23);
            this.btnRemoveFiles.TabIndex = 8;
            this.btnRemoveFiles.UseVisualStyleBackColor = false;
            this.btnRemoveFiles.Click += new System.EventHandler(this.btnRemoveFiles_Click);
            // 
            // btnAddFiles
            // 
            this.btnAddFiles.BackColor = System.Drawing.Color.Transparent;
            this.btnAddFiles.FlatAppearance.BorderSize = 0;
            this.btnAddFiles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddFiles.Image = global::ExaminationTicket.Properties.Resources.add;
            this.btnAddFiles.Location = new System.Drawing.Point(15, 188);
            this.btnAddFiles.Name = "btnAddFiles";
            this.btnAddFiles.Size = new System.Drawing.Size(31, 23);
            this.btnAddFiles.TabIndex = 7;
            this.btnAddFiles.Tag = "";
            this.btnAddFiles.UseVisualStyleBackColor = false;
            this.btnAddFiles.Click += new System.EventHandler(this.btnAddFiles_Click);
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.btnSaveSettings;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(430, 253);
            this.ControlBox = false;
            this.Controls.Add(this.btnRemoveFiles);
            this.Controls.Add(this.btnAddFiles);
            this.Controls.Add(this.lwFiles);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSaveSettings);
            this.Controls.Add(this.btnClose);
            this.Name = "SettingsForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройки";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSaveSettings;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lwFiles;
        private System.Windows.Forms.ColumnHeader number;
        private System.Windows.Forms.ColumnHeader patchFile;
        private System.Windows.Forms.Button btnAddFiles;
        private System.Windows.Forms.Button btnRemoveFiles;
    }
}