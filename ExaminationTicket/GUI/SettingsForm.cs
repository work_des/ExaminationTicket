﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using ExaminationTicket.Model;
using ExaminationTicket.Services;

namespace ExaminationTicket.GUI
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            if (File.Exists(MainForm.PatchSettings))
            {
                //загружаем настройки из файла
                var serializer = new Serializer<List<Setting>>(MainForm.PatchSettings);
                var listSettings = serializer.ReadObjectFromJson();

                //добавляем в listView
                foreach (var item in listSettings)
                {
                    var listView = new ListViewItem(item.Number);
                    listView.SubItems.Add(item.PatchFile);
                    lwFiles.Items.Add(listView);
                }
            }
        }

        private void btnAddFiles_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                foreach (var item in openFileDialog.FileNames)
                {
                    var numberFile = (lwFiles.Items.Count + 1).ToString();

                    var listView = new ListViewItem(numberFile);
                    listView.SubItems.Add(item);
                    lwFiles.Items.Add(listView);
                }

                btnSaveSettings.Enabled = true;
            }
        }

        private void btnRemoveFiles_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lwFiles.SelectedItems)
                lwFiles.Items.Remove(item);

            //Пересчитываем номера вопросов
            for (var i = 0; i < lwFiles.Items.Count; i++)
                lwFiles.Items[i].Text = (i + 1).ToString();

            btnSaveSettings.Enabled = true;
        }

        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            var listSettings = new List<Setting>();

            foreach (ListViewItem item in lwFiles.Items)
                listSettings.Add(new Setting(item.Text, item.SubItems[1].Text));

            //сереализуем и сохраняем объект
            var serializer = new Serializer<List<Setting>>(MainForm.PatchSettings);
            serializer.WriteObjectToJson(listSettings);

            Close();
        }
    }
}