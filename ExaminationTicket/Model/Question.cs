﻿using System;
using System.Runtime.Serialization;

namespace ExaminationTicket.Model
{
    [DataContract]
    class Question : IComparable<Question>
    {
        public Question(string number, string variant, string description)
        {
            Number = number;
            Variant = variant;
            Description = description;
        }

        /// <summary>
        /// Номер вопроса в билете
        /// </summary>
        [DataMember]
        public string Number { get; set; }

        /// <summary>
        /// Номер варианта в файле с вопросами
        /// </summary>
        [DataMember]
        public string Variant { get; set; }

        /// <summary>
        /// Описание вопроса
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        public int CompareTo(Question obj)
        {
            int src = int.Parse(this.Number);
            int des = int.Parse(obj.Number);

            if (src > des) return 1;
            if (src < des) return -1;
            return 0;
        }
    }
}