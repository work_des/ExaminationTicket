﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ExaminationTicket.Model
{
    internal class Questions : List<Question>
    {
        private readonly List<Question> questionRandom = new List<Question>();

        /// <summary>
        ///     Чтение вопросов из файлов структуры вида: "НомерВаринта, Вопрос"
        ///     Каждый вопрос должен начинаться с новой строки!
        /// </summary>
        /// <param name="patchFiles">Настройки</param>
        public void ReadFilesRandom(IEnumerable<Setting> patchFiles)
        {
            foreach (var patch in patchFiles)
                foreach (var question in File.ReadAllLines(patch.PatchFile, Encoding.UTF8))
                {
                    var questionPars = question.Split(new[] {',', ' '}, 2, StringSplitOptions.RemoveEmptyEntries);

                    questionRandom.Add(new Question(patch.Number, questionPars[0], questionPars[1]));
                }

            var groups = questionRandom.GroupBy(x => x.Number);
            var counts = from @group in groups select @group.Count();

            if (counts.Distinct().Count() != 1)
                throw new Exception("Количество вопросов в файлах не совпадает или они отсутствуют!");

            var random = new Random();

            //Перемешиваем билеты в случайном порядке
            AddRange(questionRandom.OrderBy(x => random.Next()));
        }
    }


    [DataContract]
    internal class Question : IComparable<Question>
    {
        public Question(string number, string variant, string description)
        {
            Number = number;
            Variant = variant;
            Description = description;
        }

        /// <summary>
        ///     Номер вопроса в билете
        /// </summary>
        [DataMember]
        public string Number { get; set; }

        /// <summary>
        ///     Номер варианта в файле с вопросами
        /// </summary>
        [DataMember]
        public string Variant { get; set; }

        /// <summary>
        ///     Описание вопроса
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        public int CompareTo(Question obj)
        {
            var src = int.Parse(Number);
            var des = int.Parse(obj.Number);

            if (src > des) return 1;
            if (src < des) return -1;
            return 0;
        }
    }
}