﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ExaminationTicket.Model
{
    /// <summary>
    ///     Коллекция выпавших экзаменационных билетов
    /// </summary>
    internal class StudentsTickets : List<StudentTicket> { }


    /// <summary>
    ///     Экзамеционный билет
    /// </summary>
    [DataContract]
    internal class StudentTicket
    {
        public StudentTicket(string fio, Questions questions)
        {
            Fio = fio;
            QuestionsList = questions;
        }

        /// <summary>
        ///     ФИО студента
        /// </summary>
        [DataMember]
        public string Fio { get; private set; }

        /// <summary>
        ///     Вопросы студента
        /// </summary>
        [DataMember]
        public Questions QuestionsList { get; private set; }
    }
}