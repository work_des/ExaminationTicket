﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using ExaminationTicket.Model;

namespace ExaminationTicket.Services
{
    internal static class Helper
    {
        /// <summary>
        ///     Глобальный обработчик исключений
        /// </summary>
        /// <param name="action">Метод</param>
        public static void TryCatch(Action action)
        {
            try
            {
                action();
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                    e = e.InnerException;

                MessageBox.Show(e.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        ///     Экспорт экзамена в текстовый файл
        /// </summary>
        public static void ExportToTxt(Examination instanse, string savePatch)
        {
            string header = $"{DateTime.Today:dd.MM.yyyy}\r\n\r\n № ВОПРОСА | № ВАРИАНТА В ФАЙЛЕ | ВОПРОС \r\n",
                separ = $"{new string('-', 160)}\r\n";


            File.WriteAllText(savePatch, header, Encoding.UTF8);
            File.AppendAllText(savePatch, separ, Encoding.UTF8);

            foreach (var item in instanse.StudentsTickets)
            {
                File.AppendAllText(savePatch, $"{item.Fio}\r\n", Encoding.UTF8);

                foreach (var question in item.QuestionsList)
                {
                    string strQuestion =
                        $"\t{question.Number} \t      {question.Variant} \t| {question.Description} \r\n";
                    File.AppendAllText(savePatch, strQuestion, Encoding.UTF8);
                }

                File.AppendAllText(savePatch, separ, Encoding.UTF8);
            }
        }

        /// <summary>
        ///     Чтение данных из текстового файла
        /// </summary>
        public static string ReadFile(string patchFile)
        {
            string text;

            using (var stream = new FileStream(patchFile, FileMode.Open))
            {
                using (var reader = new StreamReader(stream))
                {
                    text = reader.ReadToEnd();
                }
            }

            return text;
        }
    }
}