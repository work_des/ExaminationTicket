﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;

namespace ExaminationTicket.Services
{
    /// <summary>
    ///     Сериализация и Десериализация объекта
    /// </summary>
    /// <typeparam name="T">Тип объекта</typeparam>
    internal class Serializer<T>
    {
        private readonly string patch;

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="patch">Путь к файлу который необходимо сериализовать либо десериализовать</param>
        public Serializer(string patch)
        {
            if (string.IsNullOrEmpty(patch))
                throw new Exception("Отсутствует путь");
            this.patch = patch;
        }

        /// <summary>
        ///     Сериализация объекта в JSON файл
        /// </summary>
        /// <param name="instance">Объект</param>
        public void WriteObjectToJson(T instance)
        {
            var writer = new DataContractJsonSerializer(typeof(T));
            using (var fs = new FileStream(patch, FileMode.Create))
            {
                writer.WriteObject(fs, instance);
            }
        }

        /// <summary>
        ///     Десериализация JSON файла в объект
        /// </summary>
        /// <returns>Объект</returns>
        public T ReadObjectFromJson()
        {
            var reader = new DataContractJsonSerializer(typeof(T));
            using (var fs = File.OpenRead(patch))
            {
                return (T) reader.ReadObject(fs);
            }
        }
    }
}